//
//  ViewController.swift
//  ScoreKeeperSwift
//
//  Created by Adam Farrell on 7/3/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

import UIKit
class ViewController: UIViewController, UITextFieldDelegate {
    let player1Name: UITextField = UITextField();
    let player1Score: UILabel = UILabel();
    let player1Stepper: UIStepper = UIStepper();
    let player2Name: UITextField = UITextField();
    let player2Score: UILabel = UILabel();
    let player2Stepper: UIStepper = UIStepper();
    let scoreTitle: UILabel = UILabel();
    let resetBtn: UIButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton;
    let newGameBtn: UIButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton;
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor(red: 166/255, green: 172/255, blue: 245/255, alpha: 1.0);
        
        scoreTitle.text = "Score Keeper";
        scoreTitle.frame = CGRectMake(30, 30, self.view.frame.width - 60, 30);
        scoreTitle.font = UIFont.systemFontOfSize(30);
        scoreTitle.textAlignment = NSTextAlignment.Center;
        self.view.addSubview(scoreTitle);
        //Begin Player 1
        //Name
        player1Name.frame = CGRectMake(60, 120, self.view.frame.width - 120, 30);
        player1Name.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0);
        player1Name.delegate = self;
        player1Name.placeholder = "Player 1";
        player1Name.textAlignment = NSTextAlignment.Center;
        player1Name.borderStyle = UITextBorderStyle.Bezel;
        self.view.addSubview(player1Name);
        //Score
        player1Score.frame = CGRectMake(100, 170, self.view.frame.width - 200, 50);
        player1Score.backgroundColor = UIColor(red: 166/255, green: 172/255, blue: 245/255, alpha: 1.0);
        player1Score.text = "0";
        player1Score.textAlignment = NSTextAlignment.Center;
        player1Score.font = UIFont.systemFontOfSize(50);
        self.view.addSubview(player1Score);
        //Incrementer
        player1Stepper.frame = CGRectMake(140, 250, self.view.frame.width - 280, 30);
        player1Stepper.maximumValue = 21;
        player1Stepper.addTarget(self, action: "player1ChangeScore:", forControlEvents: UIControlEvents.ValueChanged);
        player1Stepper.tintColor = UIColor(red: 76/255, green: 87/255, blue: 245/255, alpha: 1.0);
        self.view.addSubview(player1Stepper);
        
        
        //Begin Player 2
        //Name
        player2Name.frame = CGRectMake(60, 340, self.view.frame.width - 120, 30);
        player2Name.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0);
        player2Name.delegate = self;
        player2Name.placeholder = "Player 2";
        player2Name.textAlignment = NSTextAlignment.Center;
        player2Name.borderStyle = UITextBorderStyle.Bezel;
        self.view.addSubview(player2Name);
        //Score
        player2Score.frame = CGRectMake(100, 390, self.view.frame.width - 200, 50);
        player2Score.backgroundColor = UIColor(red: 166/255, green: 172/255, blue: 245/255, alpha: 1.0);
        player2Score.text = "0";
        player2Score.textAlignment = NSTextAlignment.Center;
        player2Score.font = UIFont.systemFontOfSize(50);
        self.view.addSubview(player2Score);
        //Incrementer
        player2Stepper.frame = CGRectMake(140, 470, self.view.frame.width - 280, 30);
        player2Stepper.maximumValue = 21;
        player2Stepper.addTarget(self, action: "player2ChangeScore:", forControlEvents: UIControlEvents.ValueChanged);
        player2Stepper.tintColor = UIColor(red: 76/255, green: 87/255, blue: 245/255, alpha: 1.0);
        self.view.addSubview(player2Stepper);
        
        newGameBtn.frame = CGRectMake(80, 550, self.view.frame.width - 160, 40);
        newGameBtn.backgroundColor = UIColor(red: 76/255, green: 87/255, blue: 245/255, alpha: 1.0);
        newGameBtn.setTitle("New Game", forState: UIControlState.Normal);
        newGameBtn.setTitleColor(UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0), forState: UIControlState.Normal);
        newGameBtn.addTarget(self, action: "newGameBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside);
        self.view.addSubview(newGameBtn);
        
        resetBtn.frame = CGRectMake(80, 600, self.view.frame.width - 160, 40);
        resetBtn.backgroundColor = UIColor(red: 76/255, green: 87/255, blue: 245/255, alpha: 1.0);
        resetBtn.setTitle("Reset", forState: UIControlState.Normal);
        resetBtn.setTitleColor(UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0), forState: UIControlState.Normal);
        resetBtn.addTarget(self, action: "resetBtnPressed:", forControlEvents: UIControlEvents.TouchUpInside);
        self.view.addSubview(resetBtn);
        
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        player1Name.resignFirstResponder();
        player2Name.resignFirstResponder();
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return false;
    }
    
    func player1ChangeScore(sender:UIStepper!) {
        player1Score.text = String(NSString(format: "%.f", sender.value));
    }
    
    func player2ChangeScore(sender:UIStepper!) {
        player2Score.text = String(NSString(format: "%.f", sender.value));
    }
    
    func newGameBtnPressed(sender:UIStepper!) {
        player1Score.text = "0";
        player1Stepper.value = 0;
        player2Score.text = "0";
        player2Stepper.value = 0;
    }
    
    func resetBtnPressed(sender:UIStepper!) {
        player1Score.text = "0";
        player1Stepper.value = 0;
        player2Score.text = "0";
        player2Stepper.value = 0;
        player1Name.text = "";
        player2Name.text = "";
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

